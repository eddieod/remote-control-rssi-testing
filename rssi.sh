#!/bin/bash

function usage() {
    cat << EOF
    rssi.sh:
        Repeats an btmgmt command to find an average bluetooth rssi value for a particular remote. Outputs also contain elapsed time, total number of devices found, total rssi and average bt pollution

        Ensure that you change the directory in which the logs and csv files will be stored.
        This parameter is stored in the 'file_naming' function


    
    Usage:
        -m|--mac            MAC address of Bluetooth device to analyse
        -r|--repeats        The Number of repeats of the 'btmgmt find' command
        -e|--env            For file naming reasons (general environment of remote and its surroundings)
        -d|--distance       Distance between remote and computer/hub
        -h|--help           Display this help message
EOF
}

# The params should be an array of bools
function parse () {
    local param_mac=false
    local param_rep=false
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -m|--mac)
                MAC="$2"
                param_mac=true
                shift
                shift
                ;;
            -r|--repeats)
                REP="$2"
                param_rep=true
                shift
                shift
                ;;
            -e|--env)
                ENVIRON="$2"
                shift
                shift
                ;;
            -d|--distance)
                DIST="$2"
                shift
                shift
                ;;
            -h|--help)
                usage
                exit 0
                ;;
            *)
                echo "Invalid parameter supplied: $key"
                usage
                exit 2
                ;;
        esac
    done
    if [ $param_mac == false ] || [ $param_rep == false ]; then
        echo "-m and -r are mandatory params"
        exit 2
    fi
}

# DESC: Increments the value of total devs and total rssi, as well as assigning a value to the remote rssi if found
# ARGS: none - using args from outside scope
# OUTS: All args get used after function has exited.
function extract_info () {
    capture_rssi=false
    is_remote=false
    # increment bluetooth dev val
    let "total_devs++"
    for word in $el; do
        if [[ $word == *"$MAC"* ]]; then
            is_remote=true
        fi
        if [[ $capture_rssi == true ]]; then
            total_rssi=$((total_rssi+word))
            if [[ $is_remote == true ]]; then
                remote_rssi=$word
            fi
        fi
        # The next element in the output is the value. So next time it loops round, the number will be captured
        if [[ $word == *"rssi"* ]]; then
            capture_rssi=true
        else
            capture_rssi=false
        fi
    done
}

function file_naming () {
    instance_time=$(date "+%d%m%y-%H%M%S")
    underscore="_"
    instance_time="$instance_time$underscore"
    ENVIRON="$ENVIRON$underscore"
    DIST="$DIST$underscore"
    DIR="/home/eddie/kraydel/remote-control-rssi-testing/files/"
    dir_exists=$(ls $DIR)
    if [[ $dir_exists == "" ]]; then
        echo "Have you changed the 'DIR' parameter in the file_naming function in the script to a place where you can access?"
        exit 2
    fi
    RESULTS_FILE=$DIR$ENVIRON$DIST$instance_time$MAC
    echo "$RESULTS_FILE"
    echo "T(s), Total Devices Found, Total RSSI (dBm), Average BT RSSI Pollution (dBm), Remote RSSI (dbm)" >> $RESULTS_FILE.csv
}

parse "$@"

echo "mac: $MAC"  # c7ba6 is A4:34:F1:96:AB:7C
echo "reps: $REP"
echo "environment: $ENVIRON"
echo "distance to remote: $DIST"

file_naming

time_start=$(date "+%s")
for ((i = 1 ; i <= $REP ; i++)); do
    echo "rep $i" | tee -a $RESULTS_FILE.log
    time_stamp=$(date "+%s")
    time_elapsed=$(( time_stamp-time_start ))
    mapfile -t all_devices < <( sudo btmgmt find )

    total_devs=0
    total_rssi=0
    remote_rssi=0

    # Loop through each found device and extract information from the output
    for el in "${all_devices[@]}"; do
        if [[ $el == *"found"* ]]; then
            echo "$el" | tee -a $RESULTS_FILE.log
            extract_info
        fi
    done

    # Factoring out the BT device of interest, what is the overall BT pollution in the current environment
    if [[ $total_devs > 1 ]]; then
        average_bt_pollution=$(( (total_rssi-remote_rssi)/(total_devs-1) ))
    else
        average_bt_pollution=0
    fi

    # Print and output to file
    echo "$time_elapsed, $total_devs, $total_rssi, $average_bt_pollution, $remote_rssi" | tee -a $RESULTS_FILE.csv
done